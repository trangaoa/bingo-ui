import './App.css';
import React, { useEffect, useRef } from 'react';
import { AiFillSetting } from "react-icons/ai";
import { BsBarChartLineFill } from "react-icons/bs";
import { BsFillQuestionCircleFill } from "react-icons/bs";
import { AiOutlineClose } from "react-icons/ai";
import { GoThreeBars } from "react-icons/go";
import * as myfn from './AppFunctions.js';
import { useState } from 'react';

function App() {
  document.documentElement.setAttribute("data-theme", "light");
  localStorage.setItem("theme", "light");
  document.documentElement.setAttribute("color-blind", "no");
  localStorage.setItem("color-blind", "no");

  const [list, setList] = useState([])
  const [userID, setUserID] = useState(0);
  const [isSelectedTurn, setIsSelectedTurn] = useState(0);
  const [score, setScore] = useState(0)
  const [isEndGame, setIsEndGame] = useState(false)
  const [timeToLeft, setTimeToLeft] = useState(0)
  const [ws, setWs] = useState()
  const [bgColor, setBgColor] = useState()

  const selectedArray = useRef([]);

  useEffect(() => {
    if (!isEndGame) {
      const timer =
        timeToLeft > 0 && setInterval(() => setTimeToLeft(timeToLeft - 1), 1000);

      if (isSelectedTurn && timeToLeft === 0) {
        for (let i = 1; i <= 25; i++) {
          let found = selectedArray.current.findIndex(ele => ele === i)

          if (found == -1) {
            onChooseNumber(i)

            break;
          }
        }

      }

      return () => clearInterval(timer);
    }
  }, [timeToLeft]);

  const onGetData = () => {
    var fname = document.getElementById("fname").value;
    var pwd = document.getElementById("password").value;
    var ip_address = document.getElementById("ipaddress").value;
    var port = document.getElementById("port").value;
    var path = document.getElementById("path").value;

    setUserID(fname)

    let url = 'ws://127.0.0.1:10637/game/bingo';
    let newUrl = `ws://${ip_address}:${port}/${path}`;
    var connection = new WebSocket(newUrl);

    console.log('connecting to server', newUrl);

    connection.onopen = () => { }

    setTimeout(() => {
      let msg = {
        type: 0,
        select: 1,
        sender: fname,
        table: [1, 2],
        pass: pwd
      }
      connection.send(JSON.stringify(msg))

      setWs(connection)
      connection.onerror = (error) => {
        console.log(error)
      };

      connection.onmessage = (message) => {
        try {
          var json = JSON.parse(message.data);
        } catch (e) {
          console.log('This doesn\'t look like a valid JSON: ', message.data);
          return;
        }
        console.log('json: ', json)

        // Kết thúc game
        if (json['type'] === 7) {
          setIsEndGame(true)
        }

        // Thay đổi lượt đánh
        if (json['type'] === 10) {
          setIsSelectedTurn(json['select'] || 0)
          if (!isEndGame) setTimeToLeft(5)
        }

        // Lấy dữ liệu bảng đấu
        if (json['table'].length > 0 && json['type'] === 11) {
          setList(json['table'])
        }

        // Kết thúc ván game, hiển thị điểm
        if (json['type'] === 12) {
          setScore(json['select'])
        }

        // 6: bạn đánh, 13: đối thủ đánh
        if (json['type'] === 13 || json['type'] === 6) {
          console.log('selected arr: ', [...selectedArray.current, json['select']])
          selectedArray.current = [...selectedArray.current, json['select']]
        }

      };
    }, 1000);
  }

  const onChooseNumber = (selection) => {
    myfn.onChoose(ws, selection, userID)
  }

  return (
    <div className="App" tabIndex="0">

      <div className="game-header">
        {/* <header> */}
        <nav className="navbar navbar-light fixed-top">
          <div className="container-fluid">
            <button className="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasDarkNavbar" aria-controls="offcanvasDarkNavbar">
              {/* <span className="navbar-toggler-icon"></span> */}
              <GoThreeBars />
            </button>
            <a className="navbar-brand" href="#">BINGO</a>
            <div className="side-buttons">
              <button className="side-buttons-icons" type="button" data-bs-toggle="modal" data-bs-target="#how-modal">
                <BsFillQuestionCircleFill size={30} />
              </button>
              <button className="side-buttons-icons px-3" type="button" data-bs-toggle="modal" data-bs-target="#statistics-modal">
                <BsBarChartLineFill size={30} />
              </button>
              <button className="side-buttons-icons" type="button" data-bs-toggle="modal" data-bs-target="#settings-modal">
                <AiFillSetting size={30} />
              </button>
            </div>
            <div className="offcanvas offcanvas-start" tabIndex="-1" id="offcanvasDarkNavbar" aria-labelledby="offcanvasDarkNavbarLabel">
              <div className="offcanvas-header">
                <h5 className="offcanvas-title" id="offcanvasDarkNavbarLabel">Games</h5>
                <button type="button" className="dismiss-button" data-bs-dismiss="offcanvas" aria-label="Close">
                  <AiOutlineClose />
                </button>
              </div>
              <div className="offcanvas-body">
                <ul className="navbar-nav justify-content-start flex-grow-1 pe-3">
                  <li className="nav-item">
                    <a className="nav-link active" aria-current="page" href="#">Home</a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">Archive</a>
                  </li>

                </ul>
              </div>
            </div>
          </div>
        </nav>

      </div>

      {/* Settings Modal */}
      <div className="modal" id="settings-modal" tabIndex="-2" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-scrollable">
          <div className="modal-content">
            <div className="modal-header">
              <div className="flex-div"></div>
              <h5 className="modal-title" id="exampleModalLabel">SETTINGS</h5>
              <div className="flex-div"></div>
              <button type="button" className="dismiss-button" data-bs-dismiss="modal" aria-label="Close"><AiOutlineClose /></button>
              {/* <button type="button" className="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close">
                <AiOutlineClose/>
              </button> */}
            </div>
            <div className="modal-body">
              <div className="settings-options">
                <div className="settings-options-texts">
                  <div>
                    Hard Mode
                  </div>
                  <div>
                    <small>
                      Any revealed hints must be used in subsequent guesses
                    </small>
                  </div>
                </div>
                <div className="form-check form-switch">
                  <input className="form-check-input" type="checkbox" id="flexSwitchCheckDefault" />
                </div>
              </div>

              <div className="settings-options">
                <div className="settings-options-texts">
                  <div>
                    Dark Theme
                  </div>
                  <div>

                  </div>
                </div>
                <div className="form-check form-switch">
                  <input className="form-check-input" type="checkbox" onClick={myfn.themeButton} id="flexSwitchCheckDefault" />
                </div>
              </div>

              <div className="settings-options">
                <div className="settings-options-texts">
                  <div>
                    High Contrast Mode
                  </div>
                  <div>
                    <small>
                      For improved color vision
                    </small>
                  </div>
                </div>
                <div className="form-check form-switch">
                  <input className="form-check-input" type="checkbox" onClick={myfn.colorBlindButton} id="flexSwitchCheckDefault" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* How to play Modal */}
      <div className="modal" id="how-modal" tabIndex="-3" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-scrollable">
          <div className="modal-content">
            <div className="modal-header pb-0">
              <div className="flex-div"></div>
              <button type="button" className="dismiss-button" data-bs-dismiss="modal" aria-label="Close">
                <AiOutlineClose />
              </button>
            </div>
            <div className="modal-body">
              <div className="how-options">
                <h3>How To Play</h3>

                <h5>Guess the Wordle in 6 tries.</h5>

                <div className="bullets">
                  <ul>
                    <li>Each guess must be a valid 5-letter word.</li>
                    <li>The color of the tiles will change to show how close your guess was to the word.</li>
                  </ul>
                </div>
                <div className="examples  mb-3">
                  <div>Examples</div>
                  <div className="how-row">
                    <div className="cube exm correct col-2">W</div>
                    <div className="cube exm col-2">E</div>
                    <div className="cube exm col-2">A</div>
                    <div className="cube exm col-2">R</div>
                    <div className="cube exm col-2">Y</div>
                  </div>
                  <div>W is in the word and in the correct spot.</div>
                </div>
                <div className="examples mb-3">
                  <div className="how-row">
                    <div className="cube exm col-2">P</div>
                    <div className="cube exm present col-2">I</div>
                    <div className="cube exm col-2">L</div>
                    <div className="cube exm col-2">L</div>
                    <div className="cube exm col-2">S</div>
                  </div>
                  <div>I is in the word but in the wrong spot.</div>
                </div>
                <div className="examples mb-3">
                  <div className="how-row">
                    <div className="cube exm col-2">V</div>
                    <div className="cube exm col-2">A</div>
                    <div className="cube exm col-2">G</div>
                    <div className="cube exm absent col-2">U</div>
                    <div className="cube exm col-2">E</div>
                  </div>
                  <div>U is not in the word in any spot.</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* Statistics Modal */}
      <div className="modal" id="statistics-modal" tabIndex="-2" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-scrollable">
          <div className="modal-content">
            <div className="modal-header pb-0">
              <div className="flex-div"></div>

              <button type="button" className="dismiss-button" data-bs-dismiss="modal" aria-label="Close"><AiOutlineClose /></button>
            </div>
            <div className="modal-body pt-0">
              <div className="statistics-options">

                <div>
                  <h5 className="modal-title" id="exampleModalLabel">STATISTICS</h5>
                </div>
                <div className="stats-boxes">
                  <div className="stats-box">
                    <div className="stats-num">
                      1
                    </div>
                    <div className="stats-text">
                      Played
                    </div>
                  </div>

                  <div className="stats-box">
                    <div className="stats-num">
                      100
                    </div>
                    <div className="stats-text">
                      Win %
                    </div>
                  </div>

                  <div className="stats-box">
                    <div className="stats-num">
                      1
                    </div>
                    <div className="stats-text">
                      Current Streak
                    </div>
                  </div>

                  <div className="stats-box">
                    <div className="stats-num">
                      1
                    </div>
                    <div className="stats-text">
                      Max Streak
                    </div>
                  </div>

                </div>

                <div className="mt-2 mb-1">
                  <h6 className="modal-title" id="exampleModalLabel">GUESS DISTRIBUTION</h6>
                  <div className="noData">
                    <p className="text-center">No Data</p>
                  </div>
                  <div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {
        list && list.length > 0 ?
          <div className="game-body">
            {
              isEndGame ?
                <p className='txtContent'>Điểm số của bạn: {score}. {score >= 5 ? 'Bạn' : 'Đối thủ'} chiến thắng.</p> :
                <p className='txtContent'>
                  Lượt đánh của {isSelectedTurn ? 'bạn' : 'đối thủ'} còn {timeToLeft} giây
                </p>
            }
            <div className="board-row">
              {
                list.map((item, index) => {
                  return (
                    <RenderItem
                      item={item}
                      index={index}
                      key={index}
                      onChooseNumber={onChooseNumber}
                      isSelectedTurn={isSelectedTurn}
                      selectedArray={selectedArray}
                    />
                  )
                })
              }
            </div>
          </div> :
          <div className='column1'>
            <div className='row1'>
              <div className='row1'>
                <label htmlFor="ipadress">IP </label>
                <input type="text" id="ipaddress" name="ipaddress" />
              </div>
              <div className='row1'>
                <label htmlFor="port">Port </label>
                <input type="text" id="port" name="port" />
              </div>
              <div className='row1'>
                <label htmlFor="path">Path </label>
                <input type="text" id="path" name="path" />
              </div>
            </div>
            <div className='row1'>
              <label htmlFor="fname">UID </label>
              <input type="text" id="fname" name="fname" />
            </div>
            <div className='row1'>
              <label htmlFor="password">Pwd </label>
              <input type="text" id="password" name="password" />
            </div>
            <input
              className='btn-submit'
              type="submit"
              value="Submit"
              onClick={() => onGetData()}
            />
          </div>
      }
    </div>
  );
}

const RenderItem = ({ item, index, onChooseNumber, isSelectedTurn, selectedArray }) => {
  let seletedList = selectedArray.current || [];
  let found = seletedList.findIndex(ele => ele === item)

  const [isSelected, setIsSelected] = useState(false)

  const onChoose = () => {
    setIsSelected(!isSelected)
  }

  useEffect(() => {
    if (found > -1) {
      setIsSelected(true)
    }
  }, [found])

  return (
    <div
      className='cell'
      style={{ background: isSelected ? '#6cdf51' : (isSelectedTurn ? 'rgb(226, 226, 238)' : 'gray') }}
      id={index}
      onClick={() => {
        if (!isSelected && isSelectedTurn) {
          onChoose()
          onChooseNumber(item)
          // myfn.onChooseNumber(item)
        }
      }}
    >
      <p>{item}</p>
    </div>
  )
}

export default App;
